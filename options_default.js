// Bookmark Menu v0.2.0
// Copyright © 2024 Boian Berberov
//
// Licensed under the EUPL-1.2 only.
// License text: https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
// SPDX-License-Identifier: EUPL-1.2

'use strict';

const options_default = {
	quick_move_ids : [ 'toolbar_____' ],
	localized_dates : false,
}
