// Bookmark Menu v0.2.0
// Copyright © 2020–2024 Boian Berberov
//
// Licensed under the European Union Public License version 1.2 only.
// License text: https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
// SPDX-License-Identifier: EUPL-1.2

// "bookmark" context for Library window supported as of FF 66
// https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/API/menus/ContextType

'use strict';

// `options_default` loaded in `manifest.json`

const top_places_folders = [
	'menu________',
	'toolbar_____',
	'unfiled_____'
]

//
// Initialization
//

browser.menus.onShown.addListener(
	async function(info, tab) {
		if ( info.hasOwnProperty('bookmarkId') )
		{
			function create_top_menu(title) {
				browser.menus.create(
					{
						id      : 'top-menu',
						title   : title,
						contexts: [ 'bookmark' ]
					}
				);
			}

			const place = ( await browser.bookmarks.get(info.bookmarkId) )[0];

			const options_local_promise = browser.storage.local.get(options_default);
			let options_local;

			switch (place.type)
			{
				case 'bookmark':
					create_top_menu( browser.i18n.getMessage('menuBookmark') );

					const bookmark_path = await FFX_Places.getPathTitles(place);
					const bookmark_path_end = bookmark_path.length - 1;

					browser.menus.create(
						{
							id       : 'parent-' + bookmark_path_end,
							title    : browser.i18n.getMessage(
								'menuPath',
								bookmark_path[bookmark_path_end].replaceAll('&', '&&')
							),
							contexts : [ 'bookmark' ],
							parentId : 'top-menu'
						}
					);

					// Early refresh
					browser.menus.refresh();

					for ( let parent_num = bookmark_path_end - 1; 0 <= parent_num; parent_num-- )
					{
						browser.menus.create(
							{
								id       : 'parent-' + parent_num,
								title    : bookmark_path[parent_num].replaceAll('&', '&&'),
								contexts : [ 'bookmark' ],
								parentId : 'parent-' + ( parent_num + 1 )
							}
						);
					}

					// Resolve options_local later for bookmarks
					options_local = await options_local_promise;

					browser.menus.create(
						{
							id       : 'date-added',
							title    : browser.i18n.getMessage(
								'menuDateAdded',
								(
									options_local.localized_dates === true ?
									( new Date(place.dateAdded) ).toLocaleDateString() :
									( new Date(place.dateAdded) ).toDateString()
								)
							),
							contexts : [ 'bookmark' ],
							parentId : 'top-menu'
						}
					);

					browser.menus.create(
						{
							id       : 'location',
							title    : browser.i18n.getMessage(
								'menuLocationInFolder',
								place.index + 1
							),
							contexts : [ 'bookmark' ],
							parentId : 'top-menu'
						}
					);

					break;
				case 'folder':
					create_top_menu( browser.i18n.getMessage('menuFolder') );

					// Resolve options_local early for folders
					options_local = await options_local_promise;

					if ( options_local.quick_move_ids.includes(place.id) )
					{
						browser.menus.create(
							{
								id       : 'remove-quick-move',
								title    : browser.i18n.getMessage('menuRemoveFromQuickMove'),
								onclick  : async function() {
									await browser.storage.local.set(
										{ quick_move_ids : options_local.quick_move_ids.filter( e => e !== place.id ) }
									);
								},
								contexts : [ 'bookmark' ],
								parentId : 'top-menu'
							}
						);
					}
					else
					{
						browser.menus.create(
							{
								id       : 'add-quick-move',
								title    : browser.i18n.getMessage('menuAddToQuickMove'),
								onclick  : async function() {
									await browser.storage.local.set(
										{ quick_move_ids : options_local.quick_move_ids.concat(place.id) }
									);
								},
								contexts : [ 'bookmark' ],
								parentId : 'top-menu'
							}
						);
					}

					// Early refresh
					browser.menus.refresh();

					break;
				case 'separator':
					return;

					break;
				default:
					console.log('place.type: ' + place.type);
					return;
			}

			// Quick Move menu

			// Skip Quick Move menu creation for top level folders
			if ( ! top_places_folders.includes(place.id) )
			{
				browser.menus.create(
					{
						id       : 'quick-move-separator',
						type     : 'separator',
						contexts : [ 'bookmark' ],
						parentId : 'top-menu'
					}
				);
				browser.menus.create(
					{
						id       : 'quick-move',
						title    : browser.i18n.getMessage('menuQuickMove'),
						contexts : [ 'bookmark' ],
						parentId : 'top-menu'
					}
				);

				if ( 0 < options_local.quick_move_ids.length )
				{
					const quick_move_places = await browser.bookmarks.get(options_local.quick_move_ids);

					for ( let quick_move_place of quick_move_places )
					{
						browser.menus.create(
							{
								id       : 'quick-move-' + quick_move_place.id,
								title    : quick_move_place.title,
								contexts : [ 'bookmark' ],
								onclick  : async function() {
									await browser.bookmarks.move(
										place.id,
										{ parentId : quick_move_place.id }
									);
								},
								parentId : 'quick-move'
							}
						);
					}
					browser.menus.create(
						{
							id       : 'reset-quick-move-separator',
							type     : 'separator',
							contexts : [ 'bookmark' ],
							parentId : 'quick-move'
						}
					);
				}

				browser.menus.create(
					{
						id       : 'reset-quick-move',
						title    : browser.i18n.getMessage('menuResetQuickMove'),
						onclick  : async function() {
							await browser.storage.local.set(
								{ quick_move_ids : options_default.quick_move_ids }
							);
						},
						contexts : [ 'bookmark' ],
						parentId : 'quick-move'
					}
				);
			}

			// Final Refresh
			browser.menus.refresh();
		}
	}
);

browser.menus.onHidden.addListener(
	function() {
		browser.menus.remove('top-menu');
		browser.menus.refresh();
	}
);

browser.bookmarks.onRemoved.addListener(
	async function(id, removeInfo) {
		if ('folder' === removeInfo.node.type)
		{
			await browser.storage.local.set(
				{
					quick_move_ids : await browser.storage.local
						.get('quick_move_ids')
						.quick_move_ids
						.filter( e => e !== id )
				}
			);
		}
	}
);
