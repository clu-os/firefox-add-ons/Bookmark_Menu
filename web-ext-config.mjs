export default {
	ignoreFiles: [
		'README.md',
		'_images/icon.png',
		'web-ext-artifacts',
		'web-ext-config.mjs',
	],

	build: {
		overwriteDest: true,
	},
};
