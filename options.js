// Bookmark Menu v0.2.0
// Copyright © 2024 Boian Berberov
//
// Licensed under the EUPL-1.2 only.
// License text: https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
// SPDX-License-Identifier: EUPL-1.2

'use strict';

window.onload = async function() {
	const localized_dates = document.getElementById('localized_dates');

	//
	// Initialize
	//

	// Load options
	const options_local = await browser.storage.local.get(options_default);
	localized_dates.checked = options_local.localized_dates;

	// Register event listeners
	localized_dates.addEventListener(
		'change',
		async function(e) {
			await browser.storage.local.set(
				{
					'localized_dates' : localized_dates.checked
				}
			)
		}
	);
}
