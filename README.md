# Bookmark Menu

> **Provides an pop-up menu, displaying the bookmark's path and location within, and allowing quick moves to a choice of folders.**

This add-on provides a pop-up menu in the Library and bookmarks toolbar.  The menu items display:
- The bookmark's path in the bookmark hierarchy
- The creation date
- The location (index) of the bookmark witin its folder

The other main function is a Quick Move menu, which allow you to move any bookmark to a predefined set of folders.

Both of these actions can be completed from Library searches and query-based folders, for which *there is no other native user interface!*

## License

Licensed under the [EUPL-1.2](https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12) only.
